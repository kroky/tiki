<div class="form-group row">
    <a class="btn btn-link" href="{service controller=manager action=index}">{icon name=list} {tr}Instances{/tr}</a>
    <a class="btn btn-link" href="{service controller=manager action=create}">{icon name=create} {tr}New Instance{/tr}</a>
    <a class="btn btn-link" href="{service controller=manager action=info}">{icon name=info} {tr}Info{/tr}</a>
</div>